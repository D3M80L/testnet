﻿namespace Shop.Models.Basket
{
    public class BasketItem
    {
        public int Id { get; set; }

        public int Count { get; set; }
    }
}