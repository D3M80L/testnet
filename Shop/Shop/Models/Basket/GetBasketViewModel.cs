﻿using System.Collections.Generic;

namespace Shop.Models.Basket
{
    public class GetBasketViewModel
    {
        public IList<BasketItem> Items { get; set; }
    }
}