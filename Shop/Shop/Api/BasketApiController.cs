using System.Linq;
using System.Net;
using System.Web.Http;
using Shop.Infrastructure.Data;
using Shop.Infrastructure.Data.Entities;
using Shop.Models.Basket;

namespace Shop.Api
{
    public sealed class BasketApiController : ApiController
    {
        private IProductRepository productRepository;
        private IBasketRepository basketRepository;

        public BasketApiController(IBasketRepository basketRepository, IProductRepository productRepository)
        {
            this.basketRepository = basketRepository;
            this.productRepository = productRepository;
        }

        public GetBasketViewModel Get()
        {
            var basketProducts = basketRepository.GetBasketProducts();
            return new GetBasketViewModel
            {
                Items = basketProducts.Select(s => new BasketItem
                {
                    Id = s.ProductId
                })
                .ToList()
            };
        }

        public BasketItem Add(int id)
        {
            if (!productRepository.Exists(id))
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            var basketProduct = basketRepository.GetBasketProducts()
                .FirstOrDefault(x => x.ProductId == id);

            if (basketProduct == null)
            {
                basketProduct = new BasketProductEntity
                {
                    ProductId = id,
                };
            }

            basketProduct.Count += 1;
            basketRepository.UpdateProduct(basketProduct);

            return new BasketItem
            {
                Id = id,
                Count = basketProduct.Count
            };
        }
    }
}