﻿namespace Shop.Infrastructure.Data
{
    public interface IProductRepository
    {
        bool Exists(int id);
    }
}