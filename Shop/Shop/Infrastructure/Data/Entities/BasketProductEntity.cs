﻿namespace Shop.Infrastructure.Data.Entities
{
    public class BasketProductEntity
    {
        public int ProductId { get; set; }

        public int Count { get; set; }
    }
}