﻿using System.Collections.Generic;
using Shop.Infrastructure.Data.Entities;

namespace Shop.Infrastructure.Data
{
    public interface IBasketRepository
    {
        IList<BasketProductEntity> GetBasketProducts();

        void UpdateProduct(BasketProductEntity entity);
    }
}