﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using FluentAssertions;
using Moq;
using Ploeh.AutoFixture;
using Shop.Api;
using Shop.Infrastructure.Data;
using Shop.Infrastructure.Data.Entities;
using Shop.Models.Basket;
using Xunit;

namespace Shop.Tests
{
    public sealed class BasketTests
    {
        private BasketApiController sut;
        private IProductRepository productRepository;
        private IBasketRepository basketRepository;
        private IFixture fixture;

        public BasketTests()
        {
            fixture = new Fixture();
            basketRepository = Mock.Of<IBasketRepository>();
            productRepository = Mock.Of<IProductRepository>();
            sut = new BasketApiController(basketRepository, productRepository);
        }

        [Fact]
        public void Given_basket_is_used_at_first_time_When_i_call_for_basket_Then_empty_basket_should_be_returned()
        {
            // Arrange
            Mock.Get(basketRepository)
                .Setup(s => s.GetBasketProducts())
                .Returns(new List<BasketProductEntity>());

            // Act
            var result = sut.Get();

            // Assert
            result.Items.Should().BeEmpty();
        }

        [Fact]
        public void Given_user_has_chosen_an_existing_product_When_i_add_product_to_basket_Then_an_representation_should_be_returned()
        {
            // Arrange
            var expectedProductId = fixture.Create<int>();
            var expectedProduct = new BasketItem()
            {
                Id = expectedProductId,
                Count = 1
            };

            Mock.Get(productRepository)
                .Setup(s => s.Exists(expectedProductId))
                .Returns(true);

            Mock.Get(basketRepository)
                .Setup(s => s.GetBasketProducts())
                .Returns(new List<BasketProductEntity>());
            
            // Act
            var result = sut.Add(expectedProductId);

            // Assert
            result.ShouldBeEquivalentTo(expectedProduct);

            Mock.Get(basketRepository)
                .Verify(v => v.UpdateProduct(It.Is<BasketProductEntity>(m => m.ProductId == expectedProductId && m.Count == 1)), Times.Once);
        }

        [Fact]
        public void Given_a_product_does_not_exist_When_i_add_this_product_to_basket_Then_an_exception_should_be_thrown()
        {
            // Arrange
            var nonExistingProduct = fixture.Create<int>();

            // Act
            Action act = () => sut.Add(nonExistingProduct);

            // Assert
            act.ShouldThrow<HttpResponseException>()
                .Which.Response.StatusCode.Should()
                .Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public void Given_i_have_one_product_in_basket_When_i_ask_for_basket_Then_the_product_should_be_returned()
        {
            // Arrange
            var expectedProductId = fixture.Create<int>();

            Mock.Get(basketRepository)
                .Setup(s => s.GetBasketProducts())
                .Returns(new List<BasketProductEntity>()
                {
                    new BasketProductEntity()
                    {
                        ProductId = expectedProductId
                    }
                });

            // Act
            var result = sut.Get();

            // Assert
            result.Items.Should()
                .NotBeEmpty()
                .And.Subject.Should()
                .ContainSingle(x => x.Id == expectedProductId);
        }

        [Fact]
        public void Given_i_have_a_product_in_basket_When_i_add_the_same_product_then_the_counter_should_increase()
        {
            // Arrange
            var expectedProductId = fixture.Create<int>();

            Mock.Get(productRepository)
                .Setup(s => s.Exists(expectedProductId))
                .Returns(true);

            Mock.Get(basketRepository)
                .Setup(s => s.GetBasketProducts())
                .Returns(new List<BasketProductEntity>()
                {
                    new BasketProductEntity()
                    {
                        ProductId = expectedProductId,
                        Count = 1
                    }
                });

            // Act
            var result = sut.Add(expectedProductId);

            // Assert
            result.Count.Should().Be(2);

            Mock.Get(basketRepository)
                .Verify(v => v.UpdateProduct(It.Is<BasketProductEntity>(m => m.ProductId == expectedProductId && m.Count == 2)), Times.Once);
        }
    }
}
